Retro Games
===========

The first computer I used was an [RC700
Piccolo](http://www.jbox.dk/rc702/emulator.shtm). It was during my
first year in high school. I would go to the computer room almost
every day after school. I learned how to program in
[COMAL](https://en.wikipedia.org/wiki/COMAL). It was learning by
doing, often by looking at and modifying the source code of programs I
had gotten from other students who frequented the computer room. Many
of the programs were games. They were simple games as they had to run
in an 80x24 text terminal.

In this repository, I'm recreating games reminiscent of what I played
on the RC700 in the high school computer room. My main target is a
Linux/UNIX terminal, but I will use different programming languages
for the implementations. I might even implement the same game in
several languages.

Although the games might be playable, you shouldn't have high
expectations, as I'm doing this for fun.

Each game will reside in a directory named after the game. Here you'll
find all the code for the game (possibly several implementations)
along with some documentation.

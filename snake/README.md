Snake
=====

I cannot remember when I first played a [snake
game](https://en.wikipedia.org/wiki/Snake_(video_game_genre)). It's a
timeless classic. I played it on my [Nokia
3310](https://en.wikipedia.org/wiki/Nokia_3310) in the mid-90s and
played it on my [Commodore
64](https://en.wikipedia.org/wiki/Commodore_64) in the late 80s. I've
also written a version in [Commodore
BASIC](https://en.wikipedia.org/wiki/Commodore_BASIC). It might even
be on a floppy disk somewhere.

In the game, you control a snake. You must avoid hitting walls and
eating the snake's tail. You score points by eating food. However,
every time you eat some food, the snake gets longer.

The snake will move by itself. But, you can only control the direction
the snake is moving. You can press the following keys to control the
direction the snake is moving:

-  `z` - move left
-  `x` - move right
-  `m` - move down
-  `k` - move up
-  `q` - quit game (no confirmation)

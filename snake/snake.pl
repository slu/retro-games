#!/usr/bin/env perl

# A simple snake game for you terminal


# INSTRUCTIONS =========================================================
#
# You control a snake. The snake will move by itself. You need to
# avoid hitting walls and eating the tail of the snake (@). Controls
# are:
#
#   z - move left
#   x - move right
#   m - move down
#   k - move up
#   q - quit game (no confirmation)
#
# Food (*) is placed in the playing area. When the snake eats food,
# you score a point, but the snake also gets longer.
#
# ======================================================================


# PRAGAMS, IMPORTS & CONSTANTS =========================================
#

# I'm using the latest Perl available at the moment. It enables
# strictures, warnings, and signatures (no longer experimental).
use v5.36;


# Built-in module usage
use English qw(-no_match_vars);
use Time::HiRes qw(usleep);


# CPAN module usage: install with
#     cpan Term::ANSIScreen Term::ReadKey Term::Size::Any
use Term::ANSIScreen qw(:color :cursor :screen :keyboard);
use Term::ReadKey;
use Term::Size::Any qw(chars);


# Constants defining the dimension of the playing area and reasons why
# the game is over.
use constant COLUMNS   => 40;
use constant ROWS      => 24;
use constant Q_PRESSED => 1;
use constant ATE_TAIL  => 2;
use constant HIT_WALL  => 3;

#
# ======================================================================


# FUNCTIONS ============================================================
#

sub pre_run_check() {
    # Check that we're running in a terminal with enough columns and
    # rows to fit the game. Exit if we're not.
    my ($columns, $rows) = chars();
    if (COLUMNS > $columns || ROWS > $rows) {
        say "I need to run in a terminal with at least ".
          COLUMNS." columns and ".ROWS." rows.";
        say "It looks like I'm running in a ${columns} by ${rows} terminal.";
        exit;
    }
}


sub is_on_snake($x, $y, $snake) {
    # return 1 if coordinates are on the snake
    # else return 0
    my $is_on_snake = 0;
    for my $pos (@$snake) {
        if ($x == $pos->[0] && $y == $pos->[1]) {
            $is_on_snake = 1;
            last;
        }
    }
    return $is_on_snake;
}


sub dump_food($snake) {
    # find random location not on snake and place food there
    state $food = colored("*", 'green');
    my ($x, $y);
    do {
        $x = int(rand(COLUMNS-2)) + 2;
        $y = int(rand(ROWS-2)) + 2;
    } while (is_on_snake($x, $y, $snake));
    print locate($y, $x), $food;
    return ($x, $y);
}


sub setup_terminal() {
    # Setup terminal for gaming: autoflush for instantly printing to
    # screen, ignore Ctrl-C (press 'q' to quit), and read raw keys
    # (i.e. without return).
    $OUTPUT_AUTOFLUSH = 1;
    $SIG{INT} = 'IGNORE';
    ReadMode('cbreak');
}


sub reset_terminal() {
    # Reset terminal: show cursor and input mode
    print "\e[?25h";
    ReadMode('restore');
}


sub init_display() {
    # Initialize display: clean screen, disable cursor, and print box of
    # blue revece space characters as the surronding walls, and dump food
    # for the snake.
    print cls(), "\e[?25l";
    my $wall = colored(" ", 'reverse blue');
    print locate(1,$_), $wall, locate(ROWS,$_), $wall for (1..COLUMNS);
    print locate($_,1), $wall, locate($_,COLUMNS), $wall for (1..ROWS);
}


sub game_over($game_over, $len) {
    # Game is over: calculate score and print message in the middle of the
    # playing area.
    my $score = $len-2;
    my ($x, $y) = (COLUMNS/2-4, ROWS/2);
    print locate($y,$x),"GAME OVER";
    print locate($y+1,$x), $game_over == 2 ? "Ate Tail!" : "Wall Hit!" if ($game_over != 1);
    print locate($y+2,$x),"Score: ", sprintf("%02d", $score);
    print locate($y+4,0),"Bye!\n";
}


sub run_game() {
    my ($head_x, $head_y) = (COLUMNS/2, ROWS/2); # Snake head coords.
    my ($delta_x, $delta_y) = (1, 0);            # Snake velocity vector
    my @snake;                                   # Snake head+body coords.
    my $len = 2;                                 # Snake total length
    my ($food_x, $food_y) = dump_food(\@snake);  # Food coordinates
    my $game_over = 0;                           # Is it game over?
    my $snake = colored("@", 'yellow');

    # Main loop: Will run until a wall is hit, the snake eats its tail, or
    # 'q' is pressed.
    do {
        print locate($head_y,$head_x), $snake;
        push @snake, [$head_y,$head_x];
        if (scalar @snake > $len) {
            my $tail = shift @snake;
            print locate($tail->[0], $tail->[1])," ";
        }
        my $key = ReadKey(-1);
        if (defined $key) {
            $delta_x = -1*($key eq "z") + 1*($key eq "x");
            $delta_y = -1*($key eq "k") + 1*($key eq "m");
            $game_over = 1*($key eq "q");
        }
        usleep(150000);
        $head_x += $delta_x; $head_y += $delta_y;    
        if (is_on_snake($head_x, $head_y, \@snake)) {
            $game_over = 2;
        } elsif ($head_x <= 1 || $head_x >= COLUMNS || $head_y <= 1 || $head_y >= ROWS) {
            $game_over = 3;
        } elsif ($head_x == $food_x && $head_y == $food_y) {
            $len++;
            ($food_x, $food_y) = dump_food(\@snake);
        }
    } while ($game_over == 0);

    game_over($game_over, $len);
}

#
# ======================================================================


# MAIN PROGRAM =========================================================
#

sub main() {
    pre_run_check();
    setup_terminal();
    init_display();
    run_game();
    reset_terminal();
}

main() if ($PROGRAM_NAME eq __FILE__);

#
# ======================================================================

1;

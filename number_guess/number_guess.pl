#!/usr/bin/env perl

# A simple number guessing game


# INSTRUCTIONS =========================================================
#
# You must guess the number picked by the computer.
#
# Run the game for further instructions.
#
# ======================================================================


# PRAGMAS, IMPORTS & CONSTANTS =========================================
#

# I'm using the latest Perl available at the moment. It enables
# strictures, warnings, and signatures (no longer experimental).
use v5.36;


# Built-in module usage
use English qw(-no_match_vars);


# CPAN module usage: install with
#     cpan Lingua::EN::Inflect Scalar::Util::Numeric
use Lingua::EN::Inflect qw(PL);
use Scalar::Util::Numeric qw(isint);

#
# ======================================================================


# FUNCTIONS ============================================================
#


sub get_max_number() {
    # Get the maximum number value as the first command line
    # argument. If no argument is specified, the default value is
    # 10. Check that the value is an integer greater or equal to
    # 10. Exit if it's not.
    #
    # Return the value.

    my $max_number = $ARGV[0] || 10;

    if (! isint($max_number) || $max_number < 10) {
        say "Please set the maximum value to an integer greater than 10.";
        exit 1;
    }

    return $max_number;
}


sub print_welcome_text() {
    # Print welcome text and instructions on how to play the game.

    say "\n ********************************************************************** ";
    say "***                     N U M B E R   G U E S S                      ***";
    say " **********************************************************************\n";

    say " This is a number-guessing game. I'll pick a number, and you'll have to";
    say " guess it. For each wrong guess, I'll tell whether the guess is too low";
    say " or too high.\n";
    say " I'll pick an integer greater than or equal to 1 and less than or equal";
    say " to a maximum value. The default maximum value is 10. You can set a new";
    say " maximum value by adding an integer greater than 10 as the command line";
    say " argument for this script.\n";
}


sub run_game($max_number) {
    # Run one round of the game.

    my $number = int(rand($max_number)) + 1;

    say "\nI've picked a number between 1 and $max_number.";
    say "Let's see if you can guess it.\n";

    my $guess = 0;
    my $guess_count = 0;

    while ($guess != $number) {
        do {
            print "Please enter your guess: ";
            chomp($guess = <STDIN>);
            if (! isint($guess) || $guess < 1 || $guess > $max_number) {
                say "I need you to enter an integer between 1 and $max_number";
                $guess = 0;
            }
        } while ($guess == 0);

        $guess_count++;

        if ($guess < $number) {
            say "Too low.";
        } elsif ($guess > $number) {
            say "Too high.";
        } else {
            printf "That's it!\nYou guessed it using %d %s.\n\n", $guess_count, PL("guess",$guess_count);
        }
    }
}


sub main_loop($max_number) {
    # Run the game repeatedly until the played wants to stop.
    #
    # Return the number of rounds played.

    my $play_game = 1;
    my $round_count = 0;

    while ($play_game) {
        $round_count++;

        run_game($max_number);

        print "Do you want to play again? [Y/n]: ";
        my $answer = <STDIN>;
        $play_game = $answer !~ /^[Nn][Oo]?$/;
    }

    return $round_count;
}


sub print_goodbye_message($round_count) {
    # Print short goodbye message.

    printf "\nYou played %d %s.\n", $round_count, PL("round", $round_count);
    say "\nThanks for playing, bye!";
}


# MAIN PROGRAM =========================================================
#

sub main() {
    my $max_number = get_max_number();
    print_welcome_text();
    my $round_count = main_loop($max_number);
    print_goodbye_message($round_count);
}

main() if ($PROGRAM_NAME eq __FILE__);

#
# ======================================================================

Number Guess
============

In the game, you must guess a number picked randomly by the
computer. When your guess is incorrect, the computer tells you whether
it's too high or too low. When you guess the number, the computer
tells you how many times you tried and allow you to try again (with a
new number).

In the standard game the computer will pick a number from 1 to 10. You
can change this by adding an integer larger that ten as an command
line argument. See "Running the Game" below for an example on how to
do that.


Running the Game
----------------

``` text
$ ./number_guess.pl
```

``` text
$ perl number_guess.pl
```

``` text
$ ./number_guess.pl 100
```
